﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailOnAudio : MonoBehaviour {

    public AudioProcessor _audioProcessor;
    public enum Mode { _8mode, _8modeBuffered, _64mode, _64modeBuffered};
    public enum AmpMode { _normalAmpMode, _bufferedAmpMode};
    [Header("Audio Mode/Band config")]
    public Mode _mode;
    public int _audioBand;
    public bool _toggleAmpMode;
    public AmpMode _ampMode;
    [Header("FX Settings")]
    public Color _trailColor;
    public Vector2 _opacityMinMax;
    private Material _trailMat;


    //hidden things
    private float _amplitudeMode;
    private float[] _audioMode;
    private TrailRenderer _trailRenderer;


    // Use this for initialization
    void Start () {
    }
    private void Awake()
    {
        _trailRenderer = GetComponent<TrailRenderer>();
        _trailMat = new Material(_trailRenderer.material);
        _trailMat.SetColor("_TintColor", _trailColor);
        _trailRenderer.material = _trailMat;
    }
    // Update is called once per frame
    void Update () {
		switch (_mode) {
            case Mode._8mode:
                _audioMode = _audioProcessor._audioBand;
                break;
            case Mode._8modeBuffered:
                _audioMode = _audioProcessor._audioBandBuffer;
                break;
            case Mode._64mode:
                _audioMode = _audioProcessor._audioBand64;
                break;
            case Mode._64modeBuffered:
                _audioMode = _audioProcessor._audioBandBuffer64;
                break;
        }
        switch (_ampMode) {
            case AmpMode._normalAmpMode:
                _amplitudeMode = _audioProcessor._Amplitude;
                break;
            case AmpMode._bufferedAmpMode:
                _amplitudeMode = _audioProcessor._AmplitudeBuffer;
                break;
        }
        if (_toggleAmpMode) {
            float opacityEvaluate = Mathf.Lerp(_opacityMinMax.x, _opacityMinMax.y, _amplitudeMode);
            _trailColor = new Color(_trailColor.r, _trailColor.g, _trailColor.b, opacityEvaluate);
        }
        else {
            float opacityEvaluate = Mathf.Lerp(_opacityMinMax.x, _opacityMinMax.y, _audioMode[_audioBand]);
            _trailColor = new Color(_trailColor.r, _trailColor.g, _trailColor.b, opacityEvaluate);
        }
	}
}
