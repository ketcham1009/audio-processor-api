﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
[RequireComponent(typeof(PostProcessingBehaviour))]

public class PostProcessingOnAudio : MonoBehaviour {

    public AudioProcessor _audioProcessor;
    public Mode _bandMode;
    public bool _ampModeToggle;
    public AmpMode _ampMode;
    public int _band;
    public bool _affectChromaticAbberation;
    public float _CAMultiplier;
    public enum Mode { _8Mode, _buffered8Mode, _64Mode, _buffered64Mode };
    public enum AmpMode { _ampMode, _bufferedAmpMode };
    private float _audioMode;
    public float _caInt;

    // Use this for initialization
    void Start () {
        var CaMod = ChromaticAberrationModel.Settings.defaultSettings;
        }

    // Update is called once per frame
    void Update() {
        //FX Modifiers
        //Chromatic Abberation Modifiers
        _caInt = _audioMode * _CAMultiplier;
        _caInt = ChromaticAberrationModel.Settings.defaultSettings.intensity;

        //Audio Mode Logic
  
        if (_ampModeToggle)
        {
            switch (_ampMode)
            {
                case AmpMode._ampMode:
                    _audioMode = _audioProcessor._Amplitude;
                    break;
                case AmpMode._bufferedAmpMode:
                    _audioMode = _audioProcessor._AmplitudeBuffer;
                    break;
            }
        }
        else
        {
            switch (_bandMode)
            {
                case Mode._8Mode:
                    _audioMode = _audioProcessor._audioBand[_band];
                    break;
                case Mode._buffered8Mode:
                    _audioMode = _audioProcessor._audioBandBuffer[_band];
                    break;
                case Mode._64Mode:
                    _audioMode = _audioProcessor._audioBand64[_band];
                    break;
                case Mode._buffered64Mode:
                    _audioMode = _audioProcessor._audioBandBuffer64[_band];
                    break;
            }
        }
        //End Audio Mode Logic


    }
}
