﻿using System.Collections;
using UnityEngine;

[RequireComponent (typeof (Light))]

public class LightingOnAudio : MonoBehaviour {
    public AudioProcessor _audioProcessor;
    [Header("Audio Mode Options")]
    public Mode _bandMode;
    public int _band;
    public bool _ampModeToggle;
    public AmpMode _ampMode;
    [Header("Color Mode Options")]
    public ColorMode _colorMode;
    public float _minIntensity, _maxIntensity;
    public Color _color;
    public Gradient _gradient;
    public float _gradientModifier;
    private Color _pColor;
    Light _light;
    public enum AmpMode { _ampMode, _bufferedAmpMode};
    public enum Mode { _8Mode, _buffered8Mode, _random8Mode, _64Mode, _buffered64Mode, _random64Mode};
    public enum ColorMode { _singleColor, _scrollingGradient, _constantScrollingGradient};
    private float _audioMode;
    private float _time;
    // Use this for initialization
    void Start () {
        _light = GetComponent<Light>();
	}

    // Update is called once per frame
    void Update() {
        //Gradient Time Logic
        _time = _time + (Time.deltaTime / _gradientModifier);
        if (_time >= 1)
        {
            _time = 0;
        }

        //Audio Mode Logic
        if (_ampModeToggle)
        {
            switch (_ampMode)
            {
                case AmpMode._ampMode:
                    _audioMode = _audioProcessor._Amplitude;
                    break;
                case AmpMode._bufferedAmpMode:
                    _audioMode = _audioProcessor._AmplitudeBuffer;
                    break;
            }
        }
        else
        {
            switch (_bandMode)
            {
                case Mode._8Mode:
                    _audioMode = _audioProcessor._audioBand[_band];
                    break;
                case Mode._buffered8Mode:
                    _audioMode = _audioProcessor._audioBandBuffer[_band];
                    break;
                case Mode._random8Mode:
                    _audioMode = _audioProcessor._audioBandBuffer[Random.Range(0, 8)];
                    break;
                case Mode._64Mode:
                    _audioMode = _audioProcessor._audioBand64[_band];
                    break;
                case Mode._buffered64Mode:
                    _audioMode = _audioProcessor._audioBandBuffer64[_band];
                    break;
                case Mode._random64Mode:
                    _audioMode = _audioProcessor._audioBandBuffer64[Random.Range(0, 64)];
                    break;
            }
        }

        //Set up Color Mode
        switch (_colorMode)
        {
            case ColorMode._singleColor:
                _pColor = _color;
                break;
            case ColorMode._constantScrollingGradient:
                _pColor = _gradient.Evaluate(_time);
                break;
            case ColorMode._scrollingGradient:
                _pColor = _gradient.Evaluate(_audioMode);
                break;
        }
        // change light based on variables
        _light.color = _pColor;
        _light.intensity = _audioMode * (_maxIntensity - _minIntensity) + _minIntensity; ;
    }
}
