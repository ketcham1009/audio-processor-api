﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnAudio : MonoBehaviour
{
    public AudioProcessor _audioProcessor;
    public int _audioBand;
    [Header("Audio Modes")]
    public Mode _mode;
    public bool _toggleAmpMode;
    public AmpMode _ampMode;
    private float _audioMode;
    [Header("Movement Modes")]
    public float _movementScale;
    public enum Direction { X, Y, Z };
    public Direction _movementDirection;
    public enum Mode { _8Mode, _buffered8Mode, _random8Mode, _randomBuffered8Mode, _64Mode, _buffered64Mode, _random64Mode, _randomBuffered64Mode }
    public enum AmpMode { _ampMode, _bufferedAmpMode }
    // Use this for initialization
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        //setup audio modes
        if (_toggleAmpMode) {
            switch (_ampMode)
            {
                case AmpMode._ampMode:
                    _audioMode = _audioProcessor._Amplitude;
                    break;
                case AmpMode._bufferedAmpMode:
                    _audioMode = _audioProcessor._AmplitudeBuffer;
                    break;
            }
        }
        else {
            switch (_mode)
            {
                case Mode._8Mode:
                    _audioMode = _audioProcessor._audioBand[_audioBand];
                    break;
                case Mode._buffered8Mode:
                    _audioMode = _audioProcessor._audioBandBuffer[_audioBand];
                    break;
                case Mode._random8Mode:
                    _audioMode = _audioProcessor._audioBand[Random.Range(0, 8)];
                    break;
                case Mode._randomBuffered8Mode:
                    _audioMode = _audioProcessor._audioBandBuffer[Random.Range(0, 8)];
                    break;
                case Mode._64Mode:
                    _audioMode = _audioProcessor._audioBand64[_audioBand];
                    break;
                case Mode._buffered64Mode:
                    _audioMode = _audioProcessor._audioBandBuffer64[_audioBand];
                    break;
                case Mode._random64Mode:
                    _audioMode = _audioProcessor._audioBand64[Random.Range(0, 64)];
                    break;
                case Mode._randomBuffered64Mode:
                    _audioMode = _audioProcessor._audioBandBuffer64[Random.Range(0, 64)];
                    break;
            }
        }
        // set movement direction
        switch (_movementDirection)
        {
            case Direction.X:
                transform.localPosition = new Vector3((_audioMode * _movementScale), 0, 0);
                break;
            case Direction.Y:
                transform.localPosition = new Vector3(0, _audioMode * _movementScale, 0);
                break;
            case Direction.Z:
                transform.localPosition = new Vector3(0, 0, _audioMode * _movementScale);
                break;
        }
    }
}