﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (ParticleSystem))]

public class ParticleOnAudio : MonoBehaviour {
    public AudioProcessor _audioProcessor;
    [Header("Modes")]
    public Mode _bandMode;
    public bool _toggleAmpMode;
    public AmpMode _ampMode;
    [Header("Band")]
	public int _band;
    [Header("Scale")]
    public bool _scaleMultiplierToggle;
    public float _scaleMultiplier;
    [Header("Emission")]
    public bool _emissionMultiplierToggle;
    public float _emissionMultiplier;
    [Header("Speed")]
    public bool _speedModifierToggle;
    public float _speedModifier;
    [Header("Gravity")]
    public bool _gravityToggle;
    public bool _invertAudioGravity;
    public float _gravityModifier;
    private float _initGravity;
    [Header("Trails")]
    public bool _toggleTrailColorMode;
    public bool _toggleTrailSize;
    public float _trailSizeModifier;
    [Header("Colors")]
    public bool _colorMode;
    public ColorMode _colorType;
	public Color _particleColor;
    public Gradient _colorGradient;
    public float _gradientTime;
	public float _particleColorIntensity;
    public bool _reactionFloor;
    public float _minimumValue;
    private ParticleSystem ps;
    private float _emmInitMul;
    private Color _mainInitCol;
    private float _initScale;
    private float _time;
    public enum Mode{_8Mode, _buffered8Mode, _64Mode, _buffered64Mode };
    public enum AmpMode { _ampMode, _bufferedAmpMode};
    public enum ColorMode { _singleColorMode, _gradientMode, _constantScrollongGradientMode}
    private float _audioMode;
    private float _initStartSpeed;
    // Use this for initialization
    void Start () {
        ps = GetComponent<ParticleSystem>();
        _emmInitMul = ps.emission.rateOverTimeMultiplier;
    }

    // Update is called once per frame
    void Update() {
        var main = ps.main;
        var emis = ps.emission;
        var ptrail = ps.trails;
        var _initStartSpeed = ps.main.startSpeed;
// setup the timer
        _time = _time + (Time.deltaTime / _gradientTime);
        if (_time >= 1)
        {
            _time = 0;
        }

// setup the audio mode and audio logic
            if (_toggleAmpMode)
        {
            switch (_ampMode)
            {
                case AmpMode._ampMode:
                    _audioMode = _audioProcessor._Amplitude;
                    break;
                case AmpMode._bufferedAmpMode:
                    _audioMode = _audioProcessor._AmplitudeBuffer;
                    break;
            }
        }
        else
        {
            switch (_bandMode)
            {
                case Mode._8Mode:
                    _audioMode = _audioProcessor._audioBand[_band];
                    break;
                case Mode._buffered8Mode:
                    _audioMode = _audioProcessor._audioBandBuffer[_band];
                    break;
                case Mode._64Mode:
                    _audioMode = _audioProcessor._audioBand64[_band];
                    break;
                case Mode._buffered64Mode:
                    _audioMode = _audioProcessor._audioBandBuffer64[_band];
                    break;
            }
        }
//PARTICLE SYSTEM MAIN SECTION
//Set color brightness based on audio
        {
            if (_colorMode) {
                switch (_colorType)
                {
                    case ColorMode._singleColorMode:
                        if (_minimumValue > _audioMode && _reactionFloor == true)
                        {
                            main.startColor = new Color(0, 0, 0, 0);
                        }
                        else
                        {
                            main.startColor = ((_particleColor * _particleColorIntensity) * _audioMode);
                        }
                        break;
                    case ColorMode._gradientMode:
                        if (_minimumValue > _audioMode && _reactionFloor == true)
                        {
                            main.startColor = new Color(0, 0, 0, 0);
                        }
                        else
                        {
                            main.startColor = _colorGradient.Evaluate(_audioMode);
                        }
                        break;
                    case ColorMode._constantScrollongGradientMode:
                        if (_minimumValue > _audioMode && _reactionFloor == true)
                        {
                            main.startColor = new Color(0, 0, 0, 0);
                        }
                        else
                        {
                            main.startColor = _colorGradient.Evaluate(_time);
                        }
                        break;
                }
                
            }
            else {
                main.startColor = main.startColor;
            }
//Set particle Garvity
            if (_gravityToggle)
            {
                main.gravityModifier = _gravityModifier * _audioMode;
            }
            if (_gravityToggle == true && _invertAudioGravity == true)
            {
                main.gravityModifier = _gravityModifier * -_audioMode;
            }
            else
            {
                main.gravityModifier = main.gravityModifier;
            }
//set particle speed
            if (_speedModifierToggle)
            {
                main.startSpeed = _speedModifier * _audioMode;
            }
            else
            {
                main.startSpeed = main.startSpeed;
            }
        }

//PARTICLE SYSTEM EMISSION SECTION
//Set emissive rate based on audio
        {
            if (_emissionMultiplierToggle) {
                emis.rateOverTimeMultiplier = _emmInitMul + ( _emissionMultiplier * (_audioMode * _emmInitMul));
            }
            else {
                emis.rateOverTimeMultiplier = emis.rateOverTimeMultiplier;
            }
        }
//PARTICLE SYSTEM SCALE SECTION
//Set scale rate based on audio
        {
            if (_scaleMultiplierToggle) {
                main.startSizeMultiplier = (_initScale + (_scaleMultiplier * (_audioMode * _initScale))) - _initScale * _audioMode;
            }
            else {
                main.startSizeMultiplier = main.startSizeMultiplier;
            }
        }
//TRAIL SYSTEM SECTION
//inherit color mode
        if (_toggleTrailColorMode)
        {
            ptrail.inheritParticleColor = true;
        }
        else
        {
            ptrail.inheritParticleColor = false;
        }
//trail width mode
        if (_toggleTrailSize)
        {
            ptrail.widthOverTrail = _trailSizeModifier * _audioMode;
        }
        else
        {
            ptrail.widthOverTrail = ptrail.widthOverTrail;
        }
    }
}
