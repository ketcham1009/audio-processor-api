﻿
//values that can be called from here are
// _audioBand, _audioBandBuffer, _audioBand64, _audioBandBuffer64, _Amplitude, and _AmplitudeBuffer
//the "band" values are float[], while the "amplitude" values are just float
// bands start at 0 and end at max bands -1 (64 bands is 0 to 63)

using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

[RequireComponent (typeof (AudioSource))]
public class AudioProcessor : MonoBehaviour {
	AudioSource _audioSource;

    //Microphone input
    public AudioClip _audioClip;
    public bool _useMicrophone;
    public string _selectedDevice;
    public AudioMixerGroup _mixerGroupMicrophone, _mixerGroupMaster;


	//FFT values
	private float[] _samplesLeft = new float[512];
	private float[] _samplesRight = new float[512];
	private float[] _freqBand = new float[8];
	private float[] _bandBuffer = new float[8];
	private float[] _bufferDecrease = new float[8];
	private float[] _freqBandHighest = new float[8];

    //audio band values
    [HideInInspector]
    public float[] _audioBand, _audioBandBuffer;
    public float[] _audioBand64, _audioBandBuffer64;
    public float[] _audioBand32, _audioBandBuffer32;


    //Amplitude variables
    [HideInInspector]
	public float _Amplitude, _AmplitudeBuffer;
	private float _AmplitudeHighest;
	
	//audio profile
	private float _audioProfile;

	//stereo channels
	public enum _channel {Stereo, Left, Right};
	public _channel channel = new _channel ();

    //Audio32
    float[] _freqBand32 = new float[32];
    float[] _bandBuffer32 = new float[32];
    float[] _bufferDecrease32 = new float[32];
    float[] _freqBandHighest32 = new float[32];

    //Audio64
    float[] _freqBand64 = new float[64];
	float[] _bandBuffer64 = new float[64];
	float[] _bufferDecrease64 = new float[64];
	float[] _freqBandHighest64 = new float[64];
	//audio band64 values



    // Use this for initialization
    void Start ()
    {
        _audioProfile = 0.5f;
        _audioBand = new float[8];          //unbuffered 8 channels
		_audioBandBuffer = new float[8];    //buffered 8 channels

		_audioBand64 = new float[64];       //unbuffered 64 channels
		_audioBandBuffer64 = new float[64]; //buffered 64 channels
		_audioSource = GetComponent<AudioSource> ();
		AudioProfile (_audioProfile);

        //Microphone input
        //Uses default mic
        if (_useMicrophone) 
        {
            if (Microphone.devices.Length > 0)
            {
                _selectedDevice = Microphone.devices[0].ToString();
                _audioSource.outputAudioMixerGroup = _mixerGroupMicrophone;
                _audioSource.clip = Microphone.Start(_selectedDevice, true, 10, AudioSettings.outputSampleRate);
            }
            else
            {
                _useMicrophone = false;
            }
        }
        if (!_useMicrophone) //Normal Input
        {
            _audioSource.outputAudioMixerGroup = _mixerGroupMaster;
            _audioSource.clip = _audioClip;
        }

        _audioSource.Play();
     //   _audioSource.time += 110;
    }

	void Update ()
    {

        if (_audioSource.clip != null)
        {
            GetSpectrumAudioSource();
            MakeFrequencyBands();       // 8 channel
            BandBuffer();
            CreateAudioBands();
            MakeFrequencyBands64();     // 64 channel 
            BandBuffer64();             
            CreateAudioBands64();
            GetAmplitude();             //amplitude

        }

    }

    void AudioProfile(float audioProfile)
	{
		for (int i = 0; i < 8; i++) {
			_freqBandHighest [i] = audioProfile;
		}
	}

	void GetAmplitude()
	{
		float _CurrentAmplitude = 0;
		float _CurrentAmplitudeBuffer = 0;
		for (int i = 0; i < 8; i++) {
			_CurrentAmplitude += _audioBand [i];
			_CurrentAmplitudeBuffer += _audioBandBuffer [i];
		}
		if (_CurrentAmplitude > _AmplitudeHighest) {
			_AmplitudeHighest = _CurrentAmplitude;
		}
		_Amplitude = _CurrentAmplitude / _AmplitudeHighest;             //unbuffered amplitude
		_AmplitudeBuffer = _CurrentAmplitudeBuffer / _AmplitudeHighest; //buffered amplitude
	}

	void CreateAudioBands()
	{
		for (int i = 0; i < 8; i++) 
		{
			if (_freqBand [i] > _freqBandHighest [i]) {
				_freqBandHighest [i] = _freqBand [i];
			}
			_audioBand [i] = Mathf.Clamp((_freqBand [i] / _freqBandHighest [i]), 0, 1);
			_audioBandBuffer [i] = Mathf.Clamp((_bandBuffer [i] / _freqBandHighest [i]), 0, 1);
		}
	}

    void CreateAudioBands32()
    {
        for (int i = 0; i < 32; i++)
        {
            if (_freqBand32[i] > _freqBandHighest32[i])
            {
                _freqBandHighest32[i] = _freqBand32[i];
            }
            _audioBand32[i] = Mathf.Clamp((_freqBand32[i] / _freqBandHighest32[i]), 0, 1);
            _audioBandBuffer32[i] = Mathf.Clamp((_bandBuffer32[i] / _freqBandHighest32[i]), 0, 1);
        }
    }

    void CreateAudioBands64()
	{
		for (int i = 0; i < 64; i++) 
		{
			if (_freqBand64 [i] > _freqBandHighest64 [i]) {
				_freqBandHighest64 [i] = _freqBand64 [i];
			}
			_audioBand64 [i] = Mathf.Clamp((_freqBand64 [i] / _freqBandHighest64 [i]), 0, 1);
			_audioBandBuffer64 [i] = Mathf.Clamp((_bandBuffer64 [i] / _freqBandHighest64 [i]), 0, 1);
		}
	}

	void GetSpectrumAudioSource()
	{
		_audioSource.GetSpectrumData(_samplesLeft, 0, FFTWindow.BlackmanHarris);
		_audioSource.GetSpectrumData(_samplesRight, 1, FFTWindow.BlackmanHarris);
	}


	void BandBuffer()
	{
		for (int g = 0; g < 8; ++g) {
			if (_freqBand [g] > _bandBuffer [g]) {
				_bandBuffer [g] = _freqBand [g];
			}

			if ((_freqBand [g] < _bandBuffer [g]) && (_freqBand [g] > 0)) {
                _bufferDecrease[g] = (_bandBuffer[g] - _freqBand[g]) / 8;
                _bandBuffer[g] -= _bufferDecrease[g];

			}

		}
	}

    void BandBuffer32()//currently un-used
    {
        for (int g = 0; g < 32; ++g)
        {
            if (_freqBand32[g] > _bandBuffer32[g])
            {
                _bandBuffer32[g] = _freqBand32[g];
            }

            if ((_freqBand32[g] < _bandBuffer32[g]) && (_freqBand32[g] > 0))
            {
                _bufferDecrease32[g] = (_bandBuffer32[g] - _freqBand32[g]) / 8;
                _bandBuffer32[g] -= _bufferDecrease32[g];
            }

        }
    }

    void BandBuffer64()
	{
		for (int g = 0; g < 64; ++g) {
			if (_freqBand64 [g] > _bandBuffer64 [g]) {
				_bandBuffer64 [g] = _freqBand64 [g];
			}

			if ((_freqBand64 [g] < _bandBuffer64 [g]) && (_freqBand64 [g] > 0)) {
                _bufferDecrease64[g] = (_bandBuffer64[g] - _freqBand64[g]) / 8;
                _bandBuffer64[g] -= _bufferDecrease64 [g];
			}

		}
	}

	void MakeFrequencyBands()
	{
		int count = 0;

		for (int i = 0; i < 8; i++) {


			float average = 0;
			int sampleCount = (int)Mathf.Pow (2, i) * 2;

			if (i == 7) {
				sampleCount += 2;
			}
			for (int j = 0; j < sampleCount; j++) {
				if (channel == _channel.Stereo) {
					average += (_samplesLeft [count] + _samplesRight [count]) * (count + 1);
				}
				if (channel == _channel.Left) {
					average += _samplesLeft [count] * (count + 1);
				}
				if (channel == _channel.Right) {
					average += _samplesRight [count] * (count + 1);
				}
				count++;

			}

			average /= count;

			_freqBand [i] = average * 10;

		}
	}

    void MakeFrequencyBands32() //wip
    {

        int count = 0;
        int sampleCount = 1;
        int power = 0;
        for (int i = 0; i < 32; i++)
        {


            float average = 0;

            if (i == 16 || i == 31)
            {
                sampleCount = (int)Mathf.Pow(2.5, power);
                if (power == 3)
                {
                    sampleCount -= 2;
                }
                power++;
            }

            for (int j = 0; j < sampleCount; j++)
            {
                if (channel == _channel.Stereo)
                {
                    average += (_samplesLeft[count] + _samplesRight[count]) * (count + 1);
                }
                if (channel == _channel.Left)
                {
                    average += _samplesLeft[count] * (count + 1);
                }
                if (channel == _channel.Right)
                {
                    average += _samplesRight[count] * (count + 1);
                }
                count++;

            }

            average /= count;

            _freqBand64[i] = average * 80;

        }
    }

    void MakeFrequencyBands64()
	{
		
			int count = 0;
			int sampleCount = 1;
			int power = 0;
			for (int i = 0; i < 64; i++) {


				float average = 0;

				if (i == 16 || i == 32 || i == 40 || i == 48 || i == 56) {
					sampleCount = (int)Mathf.Pow (2, power);
					if (power == 3) {
						sampleCount -= 2;
					}
					power++;
				}

				for (int j = 0; j < sampleCount; j++) {
					if (channel == _channel.Stereo) {
						average += (_samplesLeft [count] + _samplesRight [count]) * (count + 1);
					}
					if (channel == _channel.Left) {
						average += _samplesLeft [count] * (count + 1);
					}
					if (channel == _channel.Right) {
						average += _samplesRight [count] * (count + 1);
					}
					count++;

				}

				average /= count;

				_freqBand64 [i] = average * 80;

			}
	}
}