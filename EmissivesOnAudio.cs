﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmissivesOnAudio : MonoBehaviour
{
    [Header("Band Values")]
    public AudioProcessor _audioProcessor;
    public int _band;
    public float _minimumValue; //wont light up until it reaches this value
    public bool _toggleSoftMode;
    [Header("Audio Modes")]
    public ChannelMode _channelMode;
    public bool _ampModeOn;
    public Ampmode _ampMode;
    [Header("Color Options")]
    public Colormode _colorMode;
    public Color _singleColor;
    public Gradient _gradientColor;
    public float _gradientTime;
    public float _intensity;
    private Color _color; // things not in the inspector
    private Color _randomColor;
    private int _randomBand8;
    private int _randomBand64;
    Material _material;
    public enum ChannelMode { _8Mode, _buffered8Mode, _random8Mode, _64Mode, _buffered64Mode, _random64Mode };
    public enum Ampmode { _ampMode, _bufferedAmpMode }
    public enum Colormode { _singleColorMode, _randomColorMode, _gradienScrollMode, _gradientMode }
    private float _time;
    private float _audioMode;
    private float _softValue;
    void Start()
    {
        _material = GetComponent<MeshRenderer>().materials[0];
        _randomColor = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
        _randomBand8 = Random.Range(0, 8);
        _randomBand64 = Random.Range(0, 64);

    }
    // Update is called once per frame
    void Update()
    {
        //Gradient Time Logic
        {
            _time = _time + (Time.deltaTime / _gradientTime);
            if (_time >= 1) {
                _time = 0;
            }
        }
        //Hard Minimum Logic
        if (_toggleSoftMode)
        {
            _softValue = _minimumValue;
        }
        else
        {
            _softValue = 0;
            if (_audioMode <= _minimumValue)
            {
                _color = Color.black;
            }
        }
        //Audio input settings
        {
            if (_ampModeOn) {
                switch (_ampMode) {
                    case Ampmode._ampMode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._Amplitude - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._Amplitude;
                        break;
                    case Ampmode._bufferedAmpMode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._AmplitudeBuffer - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._AmplitudeBuffer;
                        break;
                }
            }
            else {
                switch (_channelMode) {
                    case ChannelMode._8Mode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._audioBand[_band] - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._audioBand[_band];
                        break;
                    case ChannelMode._buffered8Mode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._audioBandBuffer[_band] - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._audioBandBuffer[_band];
                        break;
                    case ChannelMode._random8Mode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._audioBandBuffer[_randomBand8] - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._audioBandBuffer[_randomBand8];
                        break;
                    case ChannelMode._64Mode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._audioBand64[_band] - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._audioBand64[_band];
                        break;
                    case ChannelMode._buffered64Mode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._audioBandBuffer64[_band] - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._audioBandBuffer64[_band];
                        break;
                    case ChannelMode._random64Mode:
                        _material.SetColor("_EmissionColor", (((_audioProcessor._audioBandBuffer64[_randomBand64] - _softValue) * _color) * _intensity));
                        _audioMode = _audioProcessor._audioBandBuffer[_randomBand64];
                        break;
                }
            }
        }
        // color settings
        {
            switch (_colorMode) {
                case Colormode._singleColorMode:
                    _color = _singleColor;
                    break;
                case Colormode._randomColorMode:
                    _color = _randomColor;
                    break;
                case Colormode._gradienScrollMode:
                    _color = _gradientColor.Evaluate(_time);
                    break;
                case Colormode._gradientMode:
                    _color = _gradientColor.Evaluate(_audioMode);
                    break;
            }
        }
    }
}