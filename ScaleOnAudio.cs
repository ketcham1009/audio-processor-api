using System.Collections;
using UnityEngine;


public class ScaleOnAudio : MonoBehaviour
{

    public AudioProcessor _audioProcessor;
    public int _band;
    public bool _randomBands;
    public float _minimumValue;
    [Header("Toggle modes")]
    public Mode _mode;
    public bool _ampModeToggle;
    public AmpModeType _ampModeType;
    [Header("Scaling types (1 or 0)")]
    public float _x;
    public float _y;
    public float _z;
    [Header("Scaling options")]
    public float _minimumScaleMultiplier;
    public float _scaleMultiplier;
    [HideInInspector]
    public float _audioMode;
    [HideInInspector]
    public enum Mode { _8Mode, _buffered8Mode, _64Mode, _buffered64Mode };
    public enum AmpModeType { _ampBufferMode, _ampNormalMode}


    void Start()
    {
    }

    void Update()
    {
        //switch between amp and band modes
        if (_ampModeToggle)
        {
            switch (_ampModeType)
            {
                case AmpModeType._ampNormalMode:
                    _audioMode = _audioProcessor._Amplitude;
                    break;
                case AmpModeType._ampBufferMode:
                    _audioMode = _audioProcessor._AmplitudeBuffer;
                    break;
            }
        }
        else
        {
            switch (_mode)
            {
                case Mode._8Mode:
                    _audioMode = _audioProcessor._audioBand[_band];
                    break;
                case Mode._buffered8Mode:
                    _audioMode = _audioProcessor._audioBandBuffer[_band];
                    break;
                case Mode._64Mode:
                    _audioMode = _audioProcessor._audioBand64[_band];
                    break;
                case Mode._buffered64Mode:
                    _audioMode = _audioProcessor._audioBandBuffer64[_band];
                    break;
            }
        }

        //check minimum to see if object should be scaled and set scaling logic
        if (_minimumValue > _audioMode) {
            transform.localScale = new Vector3(_minimumScaleMultiplier, _minimumScaleMultiplier, _minimumScaleMultiplier);
        }
        else {
            transform.localScale = new Vector3((_audioMode * _scaleMultiplier) * _x + _minimumScaleMultiplier, (_audioMode * _scaleMultiplier) * _y + _minimumScaleMultiplier, (_audioMode * _scaleMultiplier) * _z + _minimumScaleMultiplier);
            
        }
    }
}