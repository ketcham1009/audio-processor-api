﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class FOVOnAudio : MonoBehaviour {
    public AudioProcessor _audioProcessor;
    public int _band;
    public ChannelMode _mode;
    public bool _toggleAmpMode;
    public AmpMode _ampMode;
    public float _initialFov;
    public float _FovModifier;
    public enum ChannelMode { _8Mode, _buffered8Mode, _64Mode, _buffered64Mode };
    public enum AmpMode { _ampMode, _bufferedAmpMode}
    public float _fov;
    public float _audio;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Camera.main.fieldOfView = _initialFov * 1 + ((_FovModifier * _audio) * _initialFov);
        _fov = _initialFov * 1 + ((_FovModifier * _audio) * _initialFov);
        // switch audio modes
        if (_toggleAmpMode)
        {
            switch (_ampMode)
            {
                case AmpMode._ampMode:
                    _audio = _audioProcessor._Amplitude;
                    break;
                case AmpMode._bufferedAmpMode:
                    _audio = _audioProcessor._AmplitudeBuffer;
                    break;
            }
        }
        else {
            switch (_mode)
            {
                case ChannelMode._8Mode:
                    _audio = _audioProcessor._audioBand[_band];
                    break;
                case ChannelMode._buffered8Mode:
                    _audio = _audioProcessor._audioBandBuffer[_band];
                    break;
                case ChannelMode._64Mode:
                    _audio = _audioProcessor._audioBand64[_band];
                    break;
                case ChannelMode._buffered64Mode:
                    _audio = _audioProcessor._audioBandBuffer64[_band];
                    break;
            }
        }
	}
}
